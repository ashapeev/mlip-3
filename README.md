# MLIP

MLIP is a software for Machine Learning Interatomic Potentials.
It has been developed at Skoltech (Moscow) by
Alexander Shapeev, Evgeny Podryabinkin, Konstantin Gubaev, and Ivan Novikov

## Licence
See [LICENSE](LICENSE)

## Prerequisties
* g++, gcc, gfortran, mpicxx
* Alterlatively: the same set for Intel compilers (tested with the 2017 version)
* make

## Compile
For full instructions see [INSTALL.md](INSTALL.md).

You might also be interested in LAMMPS-MLIP interface distributed here:
[https://gitlab.com/ivannovikov/interface-lammps-mlip-3/](https://gitlab.com/ivannovikov/interface-lammps-mlip-3/)


## Getting Started

Have a look at `doc/paper.pdf` first.

Check the usage examples at `test/examples/`

## Have questions?

Note that we'll not be able to answer all of your questions.
As a rule, we are supporting only the documented functionality of MLIP.
If you think you found a bug or an inconsistency in the documentation or usage examples,
please create a Gitlab.com issue.
